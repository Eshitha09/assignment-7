#include<stdio.h>

int main() {
    char string[1000], character;
    int count = 0;

    printf("Enter a string: ");
    fgets(string, sizeof(string), stdin);

    printf("Enter a frequency to find its frequency: ");
    scanf("%c", &character);

    for(int i = 0; string[i] != '\0'; ++i){
        if(character == string[i])
            ++count;
    }

    printf("Frequency of %c = %d, character, count");
    return 0;
}
