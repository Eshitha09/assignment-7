#include <stdio.h>

int main()
{
	int a[5][5], b[5][5], c[5][5]={0}, d[5][5]={0};
	int w,x,y,z,i,j,k;

	printf("Enter the number of rows and columns in the first matrix \n");
	scanf("%d %d", &w, &x);

	printf("Enter the number of rows and columns in the second matrix \n");
	scanf("%d %d", &y, &z);

	if(w!=y || x!=z)
		printf("Matrix addition cannot be performed as the rows and columns don't match \n");

	else if(x!=y)
		printf("Matrix multiplication cannot be performed vas the rows and columns don't match \n");

	else
	{
		printf("Enter the elements of first matrix \n");
		for (i=0; i<w; i++)
		{
			for (j=0; j<x; j++)
				scanf("%d", &a[i][j]);
			}
		printf("\n");

		printf("Enter the elements of second matrix \n");
		for (i=0; i<y; i++)
		{
			for (j=0; j<z; j++)
				scanf("%d", &b[i][j]);
			}
		printf("\n");

		for(i=0; i<w; i++)
		{
			for(j=0; j<x; j++)
				c[i][j]=a[i][j]+b[i][j];
		}

		printf("Result of matrix addition\n");
		for(i=0; i<w; i++)
		{
			for(j=0; j<x; j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}

		for(i=0;i<w;i++)
		{
			for(j=0;j<x;j++)
			{
				for(k=0;k<w;k++)
					d[i][j]=d[i][j] + a[i][k]*b[k][j];
			}
		}

		printf("Result of matrix multiplication\n");
		for(i=0;i<w;i++)
		{
			for(j=0;j<x;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
	return 0;
	}
}
